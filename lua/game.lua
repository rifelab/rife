-- RIFE: a Rule-based Interactive Fiction Interpreter
-- Game launcher.

-- Protect against accidental globals.
setmetatable(_G, {
    __index = function(_, v) error('Referenced undefined global: ' .. v) end,
    __newindex = function(_, v) error('Attempt to create global: ' .. v) end,
})

local root = arg[0]:gsub('[^/\\]*%.lua$', '')

local Interpreter = require (root .. 'terp')

-- Game class. Sets up interpreter and loads a program.
local Game = Interpreter.Class()

function Game:init(path)
    self.debugLevel = 0
    self.interpreter = Interpreter()
    local loaded, message = self.interpreter:loadPath(path)
    if not loaded then
        self:say("Can't load game " .. message)
        os.exit(1)
    end
    self.commandRule = Interpreter.Rule()
    self.commandRule:parseQueryLine('issue game command $name @args,')
    self.gameOver = false
    return self
end

-- Set interpreter debug level. Called from extracted game command.
function Game:debug(level)
    level = tonumber(level) or level == 'off' and 0 or 1
    self.interpreter.debugLevel = level
    self.debugLevel = level
    self:say(('set debug level %i.'):format(level))
end

-- Dump program rules or data. Called from extracted game command.
function Game:dump(what)
    if what == 'rules' then
        self.interpreter:dumpRules()
    elseif what == 'data' then
        self.interpreter:dumpData()
    end
end

local function printLine(line)
    io.stdout:write('  ', line, '\r\n')
end

local WRAP_COL = 76
local SPACE = 32
local BREAK = 10

local function sayFormatted(message)
    local offset = 1
    message = (message or ''):gsub('\r', '')
        :gsub('^%s*', ''):gsub('^.', string.upper)
        :gsub('\t', ' '):gsub('\n +', '\n'):gsub('\n\n+', '\r')
        :gsub('\n', ' '):gsub('\r', '\n')
        :gsub(' +', ' '):gsub('^ +', '')
        :gsub("''", '"')
        
    printLine('')
    while true do
        local broken = true
        while broken do
            broken = false
            for i = offset, offset + math.min(#message, WRAP_COL) do
                if message:byte(i) == BREAK then
                    printLine(message:sub(offset, i))
                    offset = i + 1
                    broken = true
                    break
                end
            end
        end
        if offset >= #message - WRAP_COL then
            printLine(message:sub(offset, -1))
            return
        end
        local i = offset + WRAP_COL
        while i > offset and message:byte(i) ~= SPACE do
            i = i - 1
        end
        printLine(message:sub(offset, i))
        offset = i + 1
    end
end

local function sayLiteral(message)
    io.stdout:write(message)
end

local sayFuncs = { [0] = sayFormatted, sayLiteral }

function Game:waitForEnter()
    io.stdout:write('\r\n  (press enter) ')
    if self.noWait then
        io.stdout:write('\r\n')
        return
    end
    io.stdin:read()
end

-- Print a formatted message.
-- Use word wrap and markdown-style line breaks and whitespace.
-- Called from extracted game command.
function Game:say(message, wait)
    local i = 0
    local lastOffset = 1
    for part, offset in message:gmatch('(.-)```()') do
        lastOffset = offset
        if not part:match('^%s*$') then sayFuncs[i % 2](part) end
        i = i + 1
    end
    local part = message:sub(lastOffset)
    if not part:match('^%s*$') then sayFuncs[i % 2](part) end
    if wait == 'wait' then    
        self:waitForEnter()
    end
end

-- Save game. Called from extracted game command.
function Game:save(name)
    local path = name .. '.save'
    local file = io.open(path, 'w')
    if not file then
        self:say('Oops, save failed!')
        return
    end
    local data = self.interpreter.data
    for i = 1, #data do
        file:write('"', table.concat(data[i].value, '" "'), '"\n')
    end
    self:say('Game "' .. name .. '" saved.')
    file:close()
end

-- Load game. Called from extracted game command.
function Game:load(name)
    local path = name .. '.save'
    local file = io.open(path)
    if not file then
        self:say('Game "' .. name .. '" not found.')
        return
    end
    self.interpreter.data = {}
    self.interpreter:loadFile(file)
    self:say('Game "' .. name .. '" loaded.')
    file:close()
    self:issuePlayerCommand('look')
end

-- Quit game. Called from extracted game command.
function Game:quit()
    self.gameOver = true
end

-- Exctract, execute, and remove game commands.
function Game:invokeCommands()
    local _, binds, i = self.interpreter:matchRule(self.commandRule)
    while binds do
        table.remove(self.interpreter.data, i)
        local args = binds['@args']
        self[binds['$name']](self, args[1], args[2], args[3])
        _, binds, i = self.interpreter:matchRule(self.commandRule)
    end
end

-- Handle player input, then apply rules and extract game commands.
-- Player input is converted to a tuple and appended to the dataset.
-- A `you` is inserted at the front of the tuple to prevent cheating.
function Game:issuePlayerCommand(text)
    local multi
    for command, more in text:gmatch('([^/]+)(/?)') do
        local time = os.clock()
        
        self.noWait = more == '/'
        
        if multi then
            io.stdout:write('\r\n/ ', command, '\r\n')
        else
            multi = true
        end
        
        self.interpreter:createData('you ' .. command)
        self.interpreter:applyRules()
        self:invokeCommands()
        
        if self.debugLevel > 0 then
            self:say(("(%-3.0f ms)"):format((os.clock() - time) * 1000))
        end
    end
end

-- Warmup and main loop.
function Game:run()
    -- Apply rules first to print title screen info, etc.
    -- Wait for player to hit enter; discarding input...
    self.interpreter:applyRules()
    self:invokeCommands()
    
    -- ...then issue a "look" command to show the first room.
    self:issuePlayerCommand('look')
    
    -- Main loop.
    while not self.gameOver do
        io.stdout:write('\r\n> ')
        self:issuePlayerCommand(io.stdin:read())
    end
    self:waitForEnter()
end

-- Load and run the game specified on command line.
Game(...):run()
