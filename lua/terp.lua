-- RIFE: a Rule-based Interactive Fiction Interpreter
-- Language interpreter

local _ -- Dummy variable

-- Simple class implementation.
local classMeta = {
    __call = function(proto, ...)
        local object = setmetatable({}, { __index = proto })
        if object.init then object:init(...) end
        return object
    end
}

local function Class()
    return setmetatable({}, classMeta)
end

local weakKeyMeta = { __mode = 'k' } -- for weakmaps

-- "Parser" helper functions

local sentinelPattern = '~`%s`~'
local sentinelsBySymbol = {}
local symbolsBySentinel = {}
local sentinelMatcher = sentinelPattern:format('%d+')
local symbolMatcher

local function defineSentinels(t)
    for i = 1, #t do
        local sentinel = sentinelPattern:format(i)
        local symbol = t[i]
        sentinelsBySymbol[symbol] = sentinel
        symbolsBySentinel[sentinel] = symbol
    end
    symbolMatcher = ('[%s]'):format(table.concat(t))
end

defineSentinels { ' ', ',', ';', '?', '.', '/' }

local function escapeCb(text)
    return text:gsub(symbolMatcher, sentinelsBySymbol)
end

local function unescapeCb(text)
    return text:sub(2, -2):gsub(sentinelMatcher, symbolsBySentinel)
end

local function escape(text)
    return text:gsub('%b""', escapeCb)
end

local function unescape(text)
    return text:gsub('%b""', unescapeCb)
end

local function split(text, type)
    local r = { type = type }

    for word in text:gmatch('[^ ]+') do
        r[#r + 1] = unescape(word)
    end
    return r
end

-- Check if some text represents a scalar variable
local function isScalarVariable(text)
    return text:find('^%$[%w_]+$') and true or false
end

-- Check if some text represents a list variable
local function isListVariable(text)
    return text:find('^@[%w_]+$') and true or false
end

-- Get indices for table.concat, using rules like string.sub
local function tableSubBounds(t, i, j)
    local n = #t
    i, j = tonumber(i) or 1, tonumber(j) or n
    if i < 0 then i = n + 1 + i end
    if j < 0 then j = n + 1 + j end
    if i < 1 then i = 1 end
    if j > n then j = n end
    return i, j
end

-- Like table.concat, but support negative indices
-- and correct out-of-bounds indices like string.sub
local function tableSub(t, sep, i, j)
    i, j = tableSubBounds(t, i, j)
    if i > j then return '' end
    
    return table.concat(t, sep, i, j)
end

local function tableRemove(t, i)
    t[i] = false
end

local indexExpander = '([@$][%w_]+)(#)(%d*)'
local unaryExpander = '(%$[%w_]+)%*(.)'
local trimmedExpander = '([@$][%w_]+)([+-]?%d*)([+-]?%d*)'

-- Expand all variables in some text, using values in `binds`
local function expandVariables(text, binds)
        -- variable count or index
    return text:gsub(indexExpander, function(m, num, index)
        local value = binds[m]
        if not value then return '' end
        local n = #value
        if index == '' then return n end
        if type(value) == 'table' then
            return value[index]
        else
            return value:sub(index, index)
        end
    end)
    -- convert to unary
    :gsub(unaryExpander, function(m, char)
        local value = tonumber(binds[m]:match('^%d+$'))
        if not value then return '' end
        return char:rep(value)
    end)
    -- variable, optionally trimmed
    :gsub(trimmedExpander, function(m, from, to)
        local value = binds[m]
        if not value then return '' end
        if type(value) == 'table' then
            return tableSub(value, ' ', from, to)
        else
            return value:sub(tonumber(from) or 1, tonumber(to) or -1)
        end
    end)
end

-- Concatenate line of text to a field of table `t`.
local function appendFieldText(t, field, text)
    t[field] = (t[field] and (t[field] .. '\n') or '') .. text
end

-- Rule class.
-- A rule represents a possible state transformation of the program data.
-- It has two parts, a query and a result. The query part is used to
-- check if a rule should be applied to the current program data, and to
-- determine any data that should be removed on successful application.
-- The result part represents new data produced on successful application.
local Rule = Class()

function Rule:init()
    self.query = {}
    self.result = {}
    self.hasReagents = false
end

function Rule:isEmpty()
    return #self.query == 0 and #self.result == 0
end

-- Parse a line of text representing part of a query.
-- Update this rule with the extracted information.
function Rule:parseQueryLine(text)
    text = text:gsub('%s$', '')
    appendFieldText(self, 'queryText', text)
    
    for negated, tuples, terminator
    in escape(text):gmatch('%s*(~?)%s*([^,;?]+)(.?)') do
    
        local prev = nil
    
        for tuple in tuples:gmatch('[^/]+') do
            local q
    
            if terminator == ',' then
                q = self:addReactant(tuple, prev)
            elseif terminator == ';' then
                q = self:addReagent(tuple, prev)
            elseif terminator == '?' then
                q = self:addCatalyst(tuple, prev)
            else
                error('Invalid query: ' .. text)
            end
            
            q.isNegated = negated == '~'
            prev = q
        end
    end
end

-- Parse a line of text representing part of a result.
-- Update this rule with the extracted information.
function Rule:parseResultLine(text)
    appendFieldText(self, 'resultText', text)
    for tuple in escape(text):gmatch('[^.]+') do
        self:addProduct(tuple)
    end
end

-- Check query for list matcher; store index if it exists.
-- Store default values for variables.
-- Throw error if multiple list matchers exist.
-- Throw error if default value appears twice for same variable.
local function prepareQuery(query)
    local listMatcherCount = 0
    query.default = {}
    query.scalarIndices = {}
    query.rejectExpanders = {}
    for i = 1, #query do
        if isListVariable(query[i]) then
            listMatcherCount = listMatcherCount + 1
            query.listMatcherIndex = i
        end

        if listMatcherCount > 1 then
            error("Multiple list matchers in query:\n" ..
                table.concat(query, ' '))
        end
        
        local var, default = query[i]:match('^(%$[%w_]+)|(.*)$')
        if default then
            if query.default[var] then
                error("Default value appears twice for same variable:\n" ..
                    table.concat(query, ' '))
            end
            query.default[var] = default
            query[i] = var
            query.hasDefaults = true
        end
        query.scalarIndices[i] = isScalarVariable(query[i])
        query.rejectExpanders[i] = '^' .. query[i]:gsub(indexExpander, '.*')
            :gsub(unaryExpander, '.*'):gsub(trimmedExpander, '.*') .. '$'
    end
    query.rejectCache = setmetatable({}, weakKeyMeta)
    return query
end

-- Extract a "reactant" from `text`, and add it to the rule's query part.
-- Reactants are parts of a query which are removed from the dataset
-- immediately during the successful application of a rule.
function Rule:addReactant(text, prev)
    local q = prepareQuery(split(text, 'reactant'))
    if prev then prev.next = q else self.query[#self.query + 1] = q end
    return q
end

-- Extract a "reagent" from `text`, and add it to the rule's query part.
-- Reagents are parts of a query which are removed from the dataset
-- after a rule is fully applied.
function Rule:addReagent(text, prev)
    local q = prepareQuery(split(text, 'reagent'))
    if prev then prev.next = q else self.query[#self.query + 1] = q end
    self.hasReagents = true
    return q
end

-- Extract a "catalyst" from `text`, and add it to the rule's query part.
-- Catalysts are parts of a query which are not removed from the dataset
-- when a rule is sucessfully applied.
function Rule:addCatalyst(text, prev)
    local q = prepareQuery(split(text, 'catalyst'))
    if prev then prev.next = q else self.query[#self.query + 1] = q end
    return q
end

-- Extract a "product" from `text`, and add it to the rule's result part.
-- Products represent new tuples to be appended to the dataset
-- when a rule is sucessfully applied.
function Rule:addProduct(text)
    self.result[#self.result + 1] = split(text, 'data')
end

-- Data class.
-- A tuple representing a single datapoint in the program data.
-- Used for statements appearing in the program, and for
-- products resulting from the successful application of rules.
local Data = Class()

function Data:init(value)
    self.value = value
end

function Data:isEmpty()
    return #self.value == 0
end

-- Expand all variables in the data, based on the values in `binds`.
-- Expansion of list matcher variables may result in a change in the number
-- of elements in the resulting tuple.
function Data:expandVariables(binds)
    local oldValue = self.value
    self.value = {}
    local k = 0
    for i = 1, #oldValue do
        local m, from, to = oldValue[i]
            :match('^(@[%w_]+)([+-]?%d*)([+-]?%d*)$')
        if m and binds[m] then
            -- list matcher, single element
            from, to = tableSubBounds(binds[m], from, to)
            for j = from, to do
                k = k + 1
                table.insert(self.value, k, binds[m][j])
            end
        else
            -- normal element
            k = k + 1
            self.value[k] = expandVariables(oldValue[i], binds)
        end
    end
    return self
end

-- Parse a line of text containing data statements.
-- Extract each statement and create a new data tuple from it.
-- Return a list of extracted statements.
function Data.parseLine(text)
    local statements = {}
    for statement in escape(text):gmatch('[^.]+') do
        local data = Data(split(statement, 'data'))
        statements[#statements + 1] = data
    end
    return statements
end

-- Interpreter class. Loads and runs code written in our DSL.
local Interpreter = Class()

function Interpreter:init()
    self.knownData = setmetatable({}, weakKeyMeta)
    self.rule = {}
    self.data = {}
    self.debugLevel = 0
end

-- Load and parse a program given a file path.
-- Populates the list of rules and initial program dataset.
function Interpreter:loadPath(path)
    local file, message = io.open(path, 'r')
    if not file then return nil, message end
    self:loadFile(file)
    return true
end

-- Load and parse a program given a valid file handle.
-- Populates the list of rules and initial program dataset.
function Interpreter:loadFile(file)
    local section = 'rules'
    local part = 'query'
    local currentRule = Rule()

    -- Very quick and dirty line-by-line parsing.
    local line = file:read('*l')
    while line do
        line = line:gsub('\r', '')
        -- while line has unbalanced quotes, append next line
        -- (allows multi-line quoted phrases).
        while select(2, line:gsub('"', {})) % 2 == 1 do
            line = line .. '\n' .. file:read('*l')
        end
        
        -- preprocessor directive
        if line:find('^%s*%[') then
            self:addRule(currentRule)
            currentRule = Rule()
            local command, arg = line:match('^%s*%[([%w_]+)%s+(.*)]%s*$')
            if command == 'load' then
                assert(self:loadPath(arg .. '.rife'))
            else
                error('Bad preprocessor directive: ' .. line)
            end
        -- ignore comment
        elseif line:find('^%s*#') then
        -- ignore blank line
        elseif line:find('^%s*$') then
        -- query parts have special terminators
        elseif line:find('[,;?]%s*$') then
            if part ~= 'query' then
                self:addRule(currentRule)
                currentRule = Rule()
            end
            part = 'query'
            currentRule:parseQueryLine(line)
        -- result parts have leading whitespace
        elseif line:find('^%s') then
            part = 'result'
            currentRule:parseResultLine(line)
        -- anything else is a statement
        else
            if part ~= 'data' then
                self:addRule(currentRule)
                currentRule = Rule()
            end
            part = 'data'
            local statements = Data.parseLine(line)
            for i = 1, #statements do
                self:addData(statements[i])
            end
        end
            
        line = file:read('*l')
    end
    
    self:addRule(currentRule)
end

-- Add a rule to the list of rules.
-- Called as program is being loaded.
function Interpreter:addRule(rule)
    if rule:isEmpty() then return rule end
    self.rule[#self.rule + 1] = rule
    return rule
end

-- Add a data statement to the current dataset.
-- Called as program is being loaded; may be called from host application.
function Interpreter:addData(data)
    if (not data) or data:isEmpty() then return data end
    self.data[#self.data + 1] = data
    return data
end

-- Convert text to a data statement and add it to the current dataset.
-- May be called from host application.
function Interpreter:createData(text)
    local data = Data()
    data.value = split(escape(text), 'data')
    self:addData(data)
    return data
end

-- Debug utility; dump all rules.
function Interpreter:dumpRules()
    for k, v in ipairs(self.rule) do
        print('')
        for k, v in ipairs(v.query) do
            print(v.type .. ' ', table.concat(v, ' / '))
        end
        for k, v in ipairs(v.result) do
            print('produces ', table.concat(v, ' / '))
        end
    end
end

-- Debug utility; dump all data.
function Interpreter:dumpData()
    for k, v in ipairs(self.data) do
        print(k, table.concat(v.value, ' / '))
    end
end

-- Apply a single query to a single data statement.
-- Update binds as free variables are resolved.
-- Return true if query matches statement, else false.
local function matchQuery(query, binds, statement, rejectTest)
    
    if query.rejectCache[statement] then return false end
    
    local countDiff = #statement.value - #query
    
    -- Query and statement tuples must have same element count to match
    -- when query doesn't have a list matcher.
    if not query.listMatcherIndex and countDiff ~= 0 then return false end
    
    -- When query has a list matcher, can have at most statement-1 elements
    -- (-1 allows matching empty list).
    if countDiff < -1 then return false end

    local j = 0
    for i = 1, #query do
        j = j + 1
        -- list matcher free variable
        if query.listMatcherIndex == i and not binds[query[i]] then
            local t = {}
            for k = 1, countDiff + 1 do
                t[k] = statement.value[j]
                j = j + 1
            end
            j = j - 1
            binds[query[i]] = t
        -- bound list matcher
        elseif query.listMatcherIndex == i then
            for k = 1, #binds[query[i]] do
                if binds[query[i]][k] ~= statement.value[j] then
                    return false
                end
                j = j + 1
            end
            j = j - 1
        -- scalar free variable
        elseif query.scalarIndices[i] and not binds[query[i]] then
            binds[query[i]] = statement.value[j]
        -- fail if not matched after variable expansion
        else
            local expanded = expandVariables(query[i], binds)
            if expanded ~= statement.value[j] then
            
                if not rejectTest then
                    return false
                end
                
                if expanded == query[i] then
                    return false
                end
            
                if not statement.value[j]:find(query.rejectExpanders[i]) then
                    return false
                end
                
            end
        end
    end

    return true
end

local function startQuery(from, to, data, bindsMeta, query)
    for i = from, to do
        local statement = data[i]
        if statement then
            local newBinds = setmetatable({}, bindsMeta)
            local matched = matchQuery(query, newBinds, statement)

            if matched then return i, newBinds end
        end
    end
end

-- Test a single query against the entire program dataset.
-- If a match is found, return the program data offset and new binds.
-- Otherwise, return nil and the current binds.
function Interpreter:query(query, binds, offset, start)
    local bindsMeta = { __index = binds }
    
    local i, b
    
    if offset >= start then
        i, b = startQuery(offset, #self.data, self.data, bindsMeta, query)
        if i then return i, b end
        i, b = startQuery(1, start - 1, self.data, bindsMeta, query)
        if i then return i, b end
    else
        i, b = startQuery(offset, start - 1, self.data, bindsMeta, query)
        if i then return i, b end
    end

    return nil, binds
end

function Interpreter:queryDefault(query, binds)
    local newBinds = setmetatable({}, { __index = binds })

    for i = 1, #query do
        local default = query.default[query[i]]
        -- list matcher free variable
        if query.listMatcherIndex == i and not newBinds[query[i]] then
            if default then
                newBinds[query[i]] = default
            else
                return nil, binds
            end
        -- bound list matcher
        elseif query.listMatcherIndex == i then
        -- scalar free variable
        elseif query.scalarIndices[i] and not newBinds[query[i]] then
            if default then
                newBinds[query[i]] = default
            else
                return nil, binds
            end
        end
    end
    return 0, newBinds
end

-- Test a rule against the dataset. Calls Interpreter:query.
-- If the rule matches, return the binds, matched data, and data index.
-- Otherwise, return false.
function Interpreter:matchRule(rule, starts)
    local dataIndex
    local binds = {}
    local indices = {}
    local matchedData = { matchedQueries = {}, matchedIndices = {} }
    
    local i = 1
    while i <= #rule.query do
        local query = rule.query[i]
        
        local q = query
        local alternate
        repeat
            alternate = q
            local start = starts and starts[alternate] or 1
            local offset = indices[alternate] or start
            dataIndex, binds = self:query(q, binds, offset, start)
            if not dataIndex and query.hasDefaults then
                dataIndex, binds = self:queryDefault(query, binds)
            end
            q = q.next
        until dataIndex or not q
        
        if dataIndex and not query.isNegated then
            indices[alternate] = dataIndex + 1
            matchedData[i] = self.data[dataIndex] or false
            matchedData.matchedQueries[i] = alternate
            matchedData.matchedIndices[i] = dataIndex
        elseif query.isNegated and not dataIndex then
            indices[alternate] = false
            matchedData[i] = false
            matchedData.matchedQueries[i] = false
            matchedData.matchedIndices[i] = 0
        else
            -- backtracked all the way, nothing matched
            if i <= 1 then return false end

            -- backtrack
            local m = getmetatable(binds)
            binds = m and m.__index or binds
            repeat
                indices[query] = false
                query = query.next
            until not query
            i = i - 2
        end
        i = i + 1
    end

    --_ = self.debugLevel >= 3 and print('query matched: ', rule.queryText)
    
    return matchedData, binds, dataIndex
end

-- Removes matched reagent data after applying a rule.
function Interpreter:removeReagents(matchedIndices)
    for i = 1, #matchedIndices do
        tableRemove(self.data, matchedIndices[i])
    end
end

-- Finalizes a matched rule, and appends new result data.
function Interpreter:finalizeRule(rule, binds, matchedData, starts)
    -- remove reactants
    for i = 1, #matchedData.matchedIndices do
        local q = matchedData.matchedQueries[i]
        if q and q.type == 'reactant' then
            tableRemove(self.data, matchedData.matchedIndices[i])
        end
    end

    -- set catalyst start points
    if rule.hasReagents then
        for i = 1, #matchedData.matchedIndices do
            local q = matchedData.matchedQueries[i]
            if q and q.type == 'catalyst' then
                starts[matchedData.matchedQueries[i]] = 
                    matchedData.matchedIndices[i] + 1
            end
        end
    end
    
    -- create data statements from products of query result,
    -- expanding any variables, and append them to the dataset.
    for i = 1, #rule.result do
        local data = Data(rule.result[i]):expandVariables(binds)
        self:addData(data)
        --_ = self.debugLevel >= 3 and print('data modified:',
        --    '+++', table.concat(data.value, ' / '))
    end
end

-- Check if two lists of datapoints are similar, based on a query.
-- This is used to determine whether a query with reagents has matched
-- all possible catalysts and wrapped back around to the first match.
-- If two datapoints at the same index in each list are different, and
-- the part of the query at that index is a catalyst, they're dissimilar.
-- Otherwise, they're similar.
local function compareMatches(a, b, query)
    if not (a and b) then return false end
    if #a ~= #b then return false end
    for i = 1, #a do
        if query[i].type == 'catalyst' and a[i] ~= b[i] then
            return false
        end
    end
    return true
end

-- Build a list of datapoints that were matched by a reagent,
-- so they can be removed after applying a rule.
local function collectReagentIndices(rule, matchedData, reagentIndices)
    for i = 1, #matchedData.matchedIndices do
        if rule.query[i].type == 'reagent' then
            reagentIndices[#reagentIndices + 1] = matchedData.matchedIndices[i]
        end
    end
    return reagentIndices
end

function Interpreter:applyRuleWithReagent(rule)
    local reagentIndices = {}
    local starts = {}
    local matchedData, binds = self:matchRule(rule, starts)
    local first = matchedData
    while matchedData do
        collectReagentIndices(rule, matchedData, reagentIndices)
        self:finalizeRule(rule, binds, matchedData, starts)
        matchedData, binds = self:matchRule(rule, starts)
        -- if the first match shows up again, stop now.
        if matchedData and compareMatches(matchedData, first, rule.query) then
            self:removeReagents(reagentIndices)
            return true
        end
    end
    if first then
        self:removeReagents(reagentIndices)
        return true
    end
    return false
end

function Interpreter:applyRuleWithoutReagent(rule)
    local matchedData, binds = self:matchRule(rule)
    if not matchedData then return false end
    self:finalizeRule(rule, binds, matchedData)
    return true
end

-- Apply the first rule that matches.
-- Returns true if a rule matched. Otherwise, return false.
function Interpreter:applyFirstMatchingRule()
    for i = 1, #self.rule do
        local rule = self.rule[i]
        if rule.hasReagents then
            if self:applyRuleWithReagent(rule) then return true end
        else
            if self:applyRuleWithoutReagent(rule) then return true end
        end
    end
    return false
end

-- Remove false values from data table.
-- Also update reject cache while we're at it.
function Interpreter:condenseData()
    local n = #self.data
    local j = 0
    for i = 1, n do
        local d = self.data[i]
        if d then
            self:buildRejectCache(d)
            j = j + 1
            self.data[j] = d
        end
    end
    while j < n do
        j = j + 1
        self.data[j] = nil
    end
end

-- Speed optimization. Each query remembers data it can't
-- possibly match, so it can reject it quickly.
function Interpreter:buildRejectCache(data)
    if self.knownData[data] then return end
    self.knownData[data] = true
    for ruleIndex = 1, #self.rule do
        local rule = self.rule[ruleIndex]
        for queryIndex = 1, #rule.query do
            local q = rule.query[queryIndex]
            repeat
                if not matchQuery(q, {}, data, true) then
                    q.rejectCache[data] = true
                end
                q = q.next
            until not q
        end
    end
end

-- Apply all applicable rules. Repeat until no more rules can be applied.
function Interpreter:applyRules()
    while self:applyFirstMatchingRule() do end
    self:condenseData()
end

Interpreter.Class = Class
Interpreter.Rule = Rule
Interpreter.Data = Data

return Interpreter
